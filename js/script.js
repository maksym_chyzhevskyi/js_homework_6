/* Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування.
Це спосіб введення в тексті певних символів, які іншим способом не
відображились би, оскільки програма сприймала би їх як керуючі символи. 

Які засоби оголошення функцій ви знаєте?
Function expression,
Function declaration,
Named Function expression

Що таке hoisting, як він працює для змінних та функцій?
Це механізм, за допомогою якого змінні та функції пересувуваються 
наверх своєї області видимості (локальної чи глобальної), незалежно від того де вони були
оголошені. Це відбуєваться до того як код буде виконано.

*/

function createNewUser() {
    const newUser = {
        firstName: prompt('What is your Name?'),
        lastName: prompt('What is your Last name?'),
        birthday: prompt('What is your date of birthday?', 'dd.mm.yyyy'),
        getLogin: function () {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
        getAge: function () {
            return new Date().getFullYear() - new Date(Date.parse(this.birthday.split('.').reverse())).getFullYear();
        },
        getPassword: function () {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6);
        }

    }
    return newUser;
}

let user1 = createNewUser();

console.log(user1);

console.log(user1.getAge());

console.log(user1.getPassword());









